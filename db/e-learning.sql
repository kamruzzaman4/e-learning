/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : e-learning

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 22/08/2020 22:20:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `admin_id` int(0) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin@gmail.com', '123456');

-- ----------------------------
-- Table structure for answer
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer`  (
  `qid` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ansid` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES ('5f41425e88fb1', '5f41425ebeb91');
INSERT INTO `answer` VALUES ('5f41425fc31df', '5f41425ff3f43');
INSERT INTO `answer` VALUES ('5f414260903f6', '5f414260c40d4');
INSERT INTO `answer` VALUES ('5f4142618b2b2', '5f414261b6a35');
INSERT INTO `answer` VALUES ('5f414262ec8ef', '5f4142637999d');
INSERT INTO `answer` VALUES ('5f41426600d59', '5f41426693268');
INSERT INTO `answer` VALUES ('5f4142695e86f', '5f414269ecc07');
INSERT INTO `answer` VALUES ('5f41426c71eb9', '5f41426d0d8b3');
INSERT INTO `answer` VALUES ('5f41426f6bda0', '5f41426fe1249');
INSERT INTO `answer` VALUES ('5f414272959e1', '5f41427310329');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, 'WEB TECHNOLOGIES', '2020-08-22 21:54:23');
INSERT INTO `course` VALUES (2, 'COMPUTER NETWORKS', '2020-08-22 21:56:28');

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history`  (
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `eid` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `score` int(0) NOT NULL,
  `level` int(0) NOT NULL,
  `right` int(0) NOT NULL,
  `wrong` int(0) NOT NULL,
  `date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of history
-- ----------------------------
INSERT INTO `history` VALUES ('user@gmail.com', '1', 30, 10, 10, 0, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options`  (
  `qid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `option` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `optionid` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES ('5f41425e88fb1', 'PHP Hypertex Processor', '5f41425ebeb84');
INSERT INTO `options` VALUES ('5f41425e88fb1', 'PHP Hyper Markup Preprocessor', '5f41425ebeb8d');
INSERT INTO `options` VALUES ('5f41425e88fb1', 'PHP Hyper Markup Processor', '5f41425ebeb90');
INSERT INTO `options` VALUES ('5f41425e88fb1', 'PHP Hypertext Preprocessor', '5f41425ebeb91');
INSERT INTO `options` VALUES ('5f41425fc31df', 'Server-side', '5f41425ff3f43');
INSERT INTO `options` VALUES ('5f41425fc31df', 'Browser-side', '5f41425ff3f4a');
INSERT INTO `options` VALUES ('5f41425fc31df', 'Client-side', '5f41425ff3f4c');
INSERT INTO `options` VALUES ('5f41425fc31df', 'In-side', '5f41425ff3f4d');
INSERT INTO `options` VALUES ('5f414260903f6', 'Rasmus Lerdorf', '5f414260c40d4');
INSERT INTO `options` VALUES ('5f414260903f6', 'Drek Kolkevi', '5f414260c40dc');
INSERT INTO `options` VALUES ('5f414260903f6', 'List Barely', '5f414260c40e0');
INSERT INTO `options` VALUES ('5f414260903f6', 'Willam Makepiece', '5f414260c40e3');
INSERT INTO `options` VALUES ('5f4142618b2b2', 'PHP can be used to develop web applications.', '5f414261b6a2d');
INSERT INTO `options` VALUES ('5f4142618b2b2', 'PHP can not be embedded into html.', '5f414261b6a35');
INSERT INTO `options` VALUES ('5f4142618b2b2', 'PHP makes a website dynamic', '5f414261b6a38');
INSERT INTO `options` VALUES ('5f4142618b2b2', 'PHP applications can not be compile', '5f414261b6a3c');
INSERT INTO `options` VALUES ('5f414262ec8ef', '?php> . . . </php>', '5f41426379990');
INSERT INTO `options` VALUES ('5f414262ec8ef', '?php . . . ?php', '5f41426379997');
INSERT INTO `options` VALUES ('5f414262ec8ef', '?php . . . ?>', '5f4142637999d');
INSERT INTO `options` VALUES ('5f414262ec8ef', '?p> . . . </p>', '5f4142637999f');
INSERT INTO `options` VALUES ('5f41426600d59', '$get', '5f4142669325b');
INSERT INTO `options` VALUES ('5f41426600d59', '$ask', '5f41426693268');
INSERT INTO `options` VALUES ('5f41426600d59', '$request', '5f4142669326b');
INSERT INTO `options` VALUES ('5f41426600d59', '$pos', '5f4142669326d');
INSERT INTO `options` VALUES ('5f4142695e86f', 'chr( );', '5f414269ecbfe');
INSERT INTO `options` VALUES ('5f4142695e86f', 'asc( );', '5f414269ecc06');
INSERT INTO `options` VALUES ('5f4142695e86f', 'ord( );', '5f414269ecc07');
INSERT INTO `options` VALUES ('5f4142695e86f', 'val( );', '5f414269ecc09');
INSERT INTO `options` VALUES ('5f41426c71eb9', 'Get', '5f41426d0d8b3');
INSERT INTO `options` VALUES ('5f41426c71eb9', 'Post', '5f41426d0d8bb');
INSERT INTO `options` VALUES ('5f41426c71eb9', 'Both', '5f41426d0d8bc');
INSERT INTO `options` VALUES ('5f41426c71eb9', 'None', '5f41426d0d8be');
INSERT INTO `options` VALUES ('5f41426f6bda0', 'ucwords($var)', '5f41426fe1249');
INSERT INTO `options` VALUES ('5f41426f6bda0', 'toupper($var)', '5f41426fe1251');
INSERT INTO `options` VALUES ('5f41426f6bda0', 'upper($var)', '5f41426fe1254');
INSERT INTO `options` VALUES ('5f41426f6bda0', 'ucword($var)', '5f41426fe1255');
INSERT INTO `options` VALUES ('5f414272959e1', 'count($variable)', '5f4142731031d');
INSERT INTO `options` VALUES ('5f414272959e1', 'len($variable)', '5f41427310324');
INSERT INTO `options` VALUES ('5f414272959e1', 'strcount($variable)', '5f41427310327');
INSERT INTO `options` VALUES ('5f414272959e1', 'strlen($variable)', '5f41427310329');

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `eid` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `qid` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `qns` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `choice` int(0) NOT NULL,
  `sn` int(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES ('1', '5f41425e88fb1', 'PHP Stands for?', 4, 1);
INSERT INTO `questions` VALUES ('1', '5f41425fc31df', 'PHP is an example of ___________ scripting language.', 4, 2);
INSERT INTO `questions` VALUES ('1', '5f414260903f6', 'Who is known as the father of PHP?', 4, 3);
INSERT INTO `questions` VALUES ('1', '5f4142618b2b2', 'Which of the following is not true?', 4, 4);
INSERT INTO `questions` VALUES ('1', '5f414262ec8ef', 'PHP scripts are enclosed within _______', 4, 5);
INSERT INTO `questions` VALUES ('1', '5f41426600d59', 'Which of the following variables is not a predefined variable?', 4, 6);
INSERT INTO `questions` VALUES ('1', '5f4142695e86f', 'When you need to obtain the ASCII value of a character which of the following function you apply in PHP?', 4, 7);
INSERT INTO `questions` VALUES ('1', '5f41426c71eb9', 'Which of the following method sends input to a script via a URL?', 4, 8);
INSERT INTO `questions` VALUES ('1', '5f41426f6bda0', 'Which of the following function returns a text in title case from a variable?', 4, 9);
INSERT INTO `questions` VALUES ('1', '5f414272959e1', 'Which of the following function returns the number of characters in a string variable?', 4, 10);

-- ----------------------------
-- Table structure for quiz
-- ----------------------------
DROP TABLE IF EXISTS `quiz`;
CREATE TABLE `quiz`  (
  `eid` int(0) NOT NULL AUTO_INCREMENT,
  `course_id` int(0) NULL DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `right` int(0) NOT NULL,
  `wrong` int(0) NOT NULL,
  `total` int(0) NOT NULL,
  `date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`eid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of quiz
-- ----------------------------
INSERT INTO `quiz` VALUES (1, 1, 'PHP Programming', 3, 1, 10, '2020-08-22 22:05:50');

-- ----------------------------
-- Table structure for rank
-- ----------------------------
DROP TABLE IF EXISTS `rank`;
CREATE TABLE `rank`  (
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `score` int(0) NOT NULL,
  `time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rank
-- ----------------------------
INSERT INTO `rank` VALUES ('user@user.com', 0, '0000-00-00 00:00:00');
INSERT INTO `rank` VALUES ('user@gmail.com', 30, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `college` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('Kamruzzaman Khondakar', 'AIUB', 'user@gmail.com', '123456');

SET FOREIGN_KEY_CHECKS = 1;
