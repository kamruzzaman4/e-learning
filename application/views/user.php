<?php include_once 'includes/header.php'; ?>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>User <small>List</small></h5>
                        </div>
                        <div class="ibox-content">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NAME</th>
                                    <th>COLLEGE</th>
                                    <th>EMAIL</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($users)) {
                                    foreach ($users as $key => $user) { ?>
                                        <tr>
                                            <td><?php echo $key + 1; ?></td>
                                            <td><?php echo $user['name']; ?></td>
                                            <td><?php echo $user['college']; ?></td>
                                            <td class="text-navy"><?php echo $user['email']; ?></td>
                                            <td><a title="Delete User" href="<?php echo base_url().'data/delete_user?demail='.$user['email']?>"><b><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></b></a></td>
                                        </tr>
                                    <?php }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include_once 'includes/footer.php'; ?>
</body>

</html>
