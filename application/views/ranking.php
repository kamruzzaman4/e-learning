<?php include_once 'includes/header.php'; ?>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ranking <small>List</small></h5>
                        </div>
                        <div class="ibox-content">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>RANK</th>
                                    <th>NAME</th>
                                    <th>COLLEGE</th>
                                    <th>EMAIL</th>
                                    <th>SCORE</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($ranking)) {
                                    foreach ($ranking as $key => $rank) { ?>
                                        <tr>
                                            <td><?php echo $key + 1; ?></td>
                                            <td><?php echo $rank['name']; ?></td>
                                            <td><?php echo $rank['college']; ?></td>
                                            <td class="text-navy"><?php echo $rank['email']; ?></td>
                                            <td class="text-navy"><?php echo $rank['score']; ?></td>
                                        </tr>
                                    <?php }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include_once 'includes/footer.php'; ?>
</body>

</html>
