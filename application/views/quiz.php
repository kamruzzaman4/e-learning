<?php include_once 'includes/header.php'; ?>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">

                        <div class="ibox-title">
                            <h5>QUIZ</h5>
                        </div>
                        <div class="ibox-content">
                            <?php echo $html; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
<?php include_once 'includes/footer.php'; ?>
</body>

</html>
