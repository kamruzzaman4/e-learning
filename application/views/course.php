<?php include_once 'includes/header.php'; ?>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox-title">
                <h5>Course Entry</h5>
            </div>
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="card-body">
                        <form id="entry-form" method="post">
                            <div class="form-group">
                                <label for="name" class="font-bold">Course Name <span style="color: red">*</span></label>
                                <input type="text" name="name" id="name" placeholder="Course Name" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="">&nbsp;</label>
                                <button class="btn btn-primary save_entry" type="button">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>List of Course</h5>
                </div>
                <div id="msg"></div>
                <div class="ibox-content">
                    <div id="delete_event_msg"></div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example tableRefresh" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-center">COURSE NAME</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                            </thead>
                            <tbody><?php if (!empty($courses)) {
                            foreach ($courses as $key => $course) { ?>
                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td class="text-center"><?php echo $course['title']; ?></td>
                                <td class="text-center">
                                    <a title="Delete" href="<?php echo base_url().'data/delete_course?id='.$course['id']?>"><b><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></b></a>
                                </td>

                            </tr>
                            <?php }
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once 'includes/footer.php'; ?>

<script>
    $('.save_entry').on('click',function(e){
        var name =  $("#name").val();
        if(name.length === 0){ alert('Please, Enter Course Name'); return false; }

        $('form#entry-form').attr('action', '<?php echo base_url()."data/course_entry" ?>');
        $('form#entry-form').submit();
        e.preventDefault();
    });
</script>
</body>

</html>
