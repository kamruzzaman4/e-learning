<?php include_once 'includes/header.php'; ?>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <?php if($_SESSION['user_type'] == 'admin'){ ?>
                    <div class="col-lg-12">
                        <div class="list-group">
                            <h1 class="list-group-item active text-center">WELCOME TO ADMIN</h1>
                        </div>
                    </div>
                <?php
                }else {
                    if (!empty($courses)) {
                        foreach ($courses as $course) {
                            ?>
                            <div class="col-lg-12">
                                <div class="ibox ">
                                    <div class="ibox-title">
                                        <h5>Course Name :: <?php echo $course['title']; ?></h5>
                                    </div>
                                    <div class="ibox-content">
                                        <table class="table table-hover">
                                            <?php if (isset($lessons[$course['id']])) { ?>
                                                <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">TOPIC</th>
                                                    <th class="text-center">TOTAL QUESTION</th>
                                                    <th class="text-center">MARKS</th>
                                                    <th class="text-center">ACTION</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i =1; foreach ($lessons[$course['id']] as $key => $lesson) { ?>
                                                    <tr>
                                                        <td class="text-center"><?php echo $i; ?></td>
                                                        <td class="text-center"><?php echo $lesson['title']; ?></td>
                                                        <td class="text-center"><?php echo $lesson['total']; ?></td>
                                                        <td class="text-center"><?php echo $lesson['total']*$lesson['right']; ?></td>
                                                        <?php if ($lesson['history'] != '') {
                                                            $url = base_url().'data/quiz?eid='.$key.'&t='.$lesson['total'].'&n=1&re=1';
                                                            $button = '<a href="'.$url.'" class="btn sub1" style="color:black;margin:0px;background:red"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Restart</b></span></a>';
                                                         }else{
                                                            $url = base_url().'data/quiz?eid='.$key.'&t='.$lesson['total'].'&n=1&re=0';
                                                            $button = '<a href="'.$url.'" class="btn sub1" style="color:black;margin:0px;background:#1de9b6"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Start</b></span></a>';
                                                         } ?>
                                                        <td class="text-center"><b><?php echo $button; ?></b></td>
                                                    </tr>
                                                <?php
                                                    $i++;
                                                }
                                            }else{ ?>
                                                <tr>
                                                    <td colspan="5" class="text-center">No Quiz Available!</td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
<?php include_once 'includes/footer.php'; ?>

<script>


    $('.generate_excel').on('click',function(e){
        var point =  $("#point").val();
        if(point.length === 0){ alert('Please, Select Point'); return false; }

        var base_month =  $("input.base_month").val();
        if(base_month.length === 0){ alert('Please, Select Base Month Date'); return false; }
        /*var base_start_date =  $("input.base_start").val();
         if(base_start_date.length === 0){ alert('Please, Select Base Start Date'); return false; }
         var base_end_date =  $("input.base_end").val();
         if(base_end_date.length === 0){ alert('Please, Select Base Start Date'); return false; }*/


        var start_date = $("input.scope_start").val();
        if(start_date.length === 0){ alert('Please, Select Scope Start Date'); return false; }
        var end_date = $("input.scope_end").val();
        if(end_date.length === 0){ alert('Please, Select Scope End Date'); return false; }

        var report_title = $(".report_title").val();
        if(report_title.length === 0){ alert('Please, Enter Report Title'); return false; }


        $('form#report-form').attr('action', '<?php echo base_url()."Report/getVolReport" ?>');
        $('form#report-form').submit();
        e.preventDefault();
    });
</script>
</body>

</html>
