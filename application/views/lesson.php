<?php include_once 'includes/header.php'; ?>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox-title">
                <h5>Lesson Entry</h5>
            </div>
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="course_name" class="font-bold">Course Name <span style="color: red">*</span></label>
                            <select id="course_id" name="course_id"  class="form-control dropDownStyle">
                                <option value="">Select Any One</option>
                                <?php if (!empty($courses)) {
                                    foreach ($courses as $key => $course) { ?>
                                        <option value="<?php echo $course['id']; ?>"><?php echo $course['title']; ?></option>
                                    <?php }
                                }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="lesson_name" class="font-bold">Lesson Name <span style="color: red">*</span></label>
                            <input type="text" name="lesson_name" id="lesson_name" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label for="no_question" class="font-bold">No. Of Questions <span style="color: red">*</span></label>
                            <input type="number" name="no_question" id="no_question" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="right_ans" class="font-bold">Marks on right answer <span style="color: red">*</span></label>
                            <input type="number" name="right_ans" id="right_ans" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="wrong_ans" class="font-bold">Minus marks on wrong answer <span style="color: red">*</span></label>
                            <input type="number" name="wrong_ans" id="wrong_ans" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="">&nbsp;</label>
                            <button class="btn btn-primary add_mcq" type="button">Confirm</button>
                        </div>
                    </div>
                    <!-- Modal Start -->
                    <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Modal title</h4>
                                </div>
                                <form id="entry-form" method="POST" action="">
                                    <div class="modal-body"></div>
                                </form>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary save_mcq">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal End -->
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>List of Lesson</h5>
                </div>
                <div id="msg"></div>
                <div class="ibox-content">
                    <div id="delete_event_msg"></div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example tableRefresh" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-center">COURSE_NAME</th>
                                <th class="text-center">LESSON_NAME</th>
                                <th class="text-center">TOTAL_QUESTION</th>
                                <th class="text-center">MARKS</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($lessons)) {
                                foreach ($lessons as $key => $lesson) { ?>
                                    <tr>
                                        <td><?php echo $key + 1; ?></td>
                                        <td class="text-center"><?php echo $lesson['courese_title']; ?></td>
                                        <td class="text-center"><?php echo $lesson['title']; ?></td>
                                        <td class="text-center"><?php echo $lesson['total']; ?></td>
                                        <td class="text-center"><?php echo $lesson['total'] * $lesson['right']; ?></td>
                                        <td class="text-center">
                                            <a title="Delete" href="<?php echo base_url() . 'data/delete_lesson?id=' . $lesson['eid'] ?>"><b><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></b></a>
                                        </td>

                                    </tr>
                                <?php }
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once 'includes/footer.php'; ?>

<script>
    $('.add_mcq').on('click',function(e){
        $('.modal-title').html('');
        $('.modal-body').html('');

        var course_id =  $("#course_id").val();
        var course_name =  $( "#course_id option:selected" ).text();
        if(course_id.length === 0){ alert('Please, Select Course'); return false; }

        var lesson_name =  $("#lesson_name").val();
        if(lesson_name.length === 0){ alert('Please, Enter Lesson Name'); return false; }

        var no_question =  $("#no_question").val();
        if(no_question.length === 0){ alert('Please, Enter No. Of Questions'); return false; }

        var right_ans =  $("#right_ans").val();
        if(right_ans.length === 0){ alert('Please, Enter marks on right answer '); return false; }

        var wrong_ans =  $("#wrong_ans").val();
        if(wrong_ans.length === 0){ alert('Please, Enter minus marks on wrong answer'); return false; }

        var htmlBody = '<input type="hidden" name="course_id" value="'+course_id+'" />'+
                       '<input type="hidden" name="lesson_name" value="'+lesson_name+'" />'+
                       '<input type="hidden" name="no_question" value="'+no_question+'" />'+
                       '<input type="hidden" name="right_ans" value="'+right_ans+'" />'+
                       '<input type="hidden" name="wrong_ans" value="'+wrong_ans+'" />';

        for(var i=1; i<=no_question; i++){
            htmlBody += '<b>Question number&nbsp;'+i+'&nbsp;:</><br />' +
            '<div class="form-group">'+
            '<label class="col-md-12 control-label" for="qns'+i+' "></label>'+
            '<div class="col-md-12">'+
            '<textarea rows="3" cols="5" name="qns'+i+'" class="form-control" placeholder="Write question number '+i+' here..."></textarea>'+
            '</div>'+
            '</div>'+
            '<div class="form-group">'+
            '<label class="col-md-12 control-label" for="'+i+'1"></label>'+
            '<div class="col-md-12">'+
            '<input id="'+i+'1" name="'+i+'1" placeholder="Enter option a" class="form-control input-md" type="text">'+
            '</div>'+
            '</div>'+
            '<div class="form-group">'+
            '<label class="col-md-12 control-label" for="'+i+'2"></label>'+
            '<div class="col-md-12">'+
            '<input id="'+i+'2" name="'+i+'2" placeholder="Enter option b" class="form-control input-md" type="text">'+
            '</div>'+
            '</div>'+
            '<div class="form-group">'+
            '<label class="col-md-12 control-label" for="'+i+'3"></label>'+
            '<div class="col-md-12">'+
            '<input id="'+i+'3" name="'+i+'3" placeholder="Enter option c" class="form-control input-md" type="text">'+
            '</div>'+
            '</div>'+
            '<div class="form-group">'+
            '<label class="col-md-12 control-label" for="'+i+'4"></label>'+
            '<div class="col-md-12">'+
            '<input id="'+i+'4" name="'+i+'4" placeholder="Enter option d" class="form-control input-md" type="text">'+
            '</div>'+
            '</div>'+
            '<br />'+
            '<b>Correct answer</b>:<br />'+
            '<select id="ans'+i+'" name="ans'+i+'" placeholder="Choose correct answer " class="form-control input-md" >'+
            '<option value="a">Select answer for question '+i+'</option>'+
            '<option value="a"> option a</option>'+
            '<option value="b"> option b</option>'+
            '<option value="c"> option c</option>'+
            '<option value="d"> option d</option> </select><br /><br />';
        }


        $('.modal-title').html(course_name+' :: '+lesson_name);
        $('.modal-body').html(htmlBody);
        e.preventDefault();

        $('#myModal').modal('show');
    });

    $('.save_mcq').on('click',function(e){
        $('form#entry-form').attr('action', '<?php echo base_url()."data/save_lesson_n_mcq" ?>');
        $('form#entry-form').submit();
    });
</script>
</body>

</html>
