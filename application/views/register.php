<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>E-Learning</title>
    <link type="image/x-icon" href="<?php echo base_url()?>asset/img/favicon.ico" rel="shortcut icon">

    <link href="<?php echo base_url()?>asset/ionicons/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>asset/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>asset/css/form.css" rel="stylesheet">
    <style type="text/css">
        body{
            width: 100%;
            background: url(<?php echo base_url()?>asset/img/book.png) ;
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }
    </style>
</head>

<body>
<section class="login first grey">
    <div class="container">
        <div class="box-wrapper">
            <div class="box box-border">
                <div class="box-body">
                    <center> <h4 style="font-family: Noto Sans;">REGISTER</h4></center><br>
                    <form id="register-form" method="POST" action="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Name :</label>
                            <input type="text" name="name" class="form-control name">
                        </div>
                        <div class="form-group">
                            <label>Email :</label>
                            <input type="email" name="email" class="form-control email">
                        </div>
                        <div class="form-group">
                            <label class="fw">Password : </label>
                            <input type="password" name="password" class="form-control password">
                        </div>
                        <div class="form-group">
                            <label>Enter Your College Name :</label>
                            <input type="text" name="college" class="form-control college">
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary btn-rounded btn-block register_button" type="button">Register</button>
                        </div>
                        <div class="form-group text-center">
                            <span class="text-muted">Already have an account!</span> <a href="<?php echo base_url() ?>">Login</a> Here..
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?php echo base_url()?>asset/js/jquery.js"></script>
<script src="<?php echo base_url()?>asset/js/bootstrap.min.js"></script>
<script>

    $('.register_button').on('click',function(e){
        var name =  $(".name").val();
        if(name.length === 0){ alert('Please, Enter Name'); return false; }
        var email =  $(".email").val();
        if(email.length === 0){ alert('Please, Enter E-mail'); return false; }

        var password =  $(".password").val();
        if(password.length === 0){ alert('Please, Enter Password'); return false; }

        $('form#register-form').attr('action', '<?php echo base_url()."Login/register" ?>');
        $('form#register-form').submit();
        e.preventDefault();
    });
</script>

</body>
</html>
