        <div class="footer">
            <div>
                <strong>Developed By <a target="_blank" href="https://www.linkedin.com/in/kamruzzaman4">© Kamruzzaman</a></strong>
            </div>
        </div>

    </div>
</div>


<!-- Mainly scripts -->
        <script src="<?php echo base_url()?>asset/js/jquery.js"></script>
<script src="<?php echo base_url()?>asset/js/popper.min.js"></script>
<script src="<?php echo base_url()?>asset/js/bootstrap.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url()?>asset/js/inspinia.js"></script>

<script>
    $(document).ready(function () {
        var str = $(location).attr('pathname');
        var n = str.lastIndexOf('/');
        var result = str.substring(n + 1);
        var active_class = (result == 'welcome') ? 'home' : result ;
        $('.'+active_class).addClass('active');
    });
</script>

</body>

</html>
