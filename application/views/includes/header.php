<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>E-Learning</title>
    <link type="image/x-icon" href="<?php echo base_url()?>asset/img/favicon.ico" rel="shortcut icon">

    <link href="<?php echo base_url()?>asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>asset/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>asset/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url()?>asset/css/style.css" rel="stylesheet">


    <style>
        .pp_img{
            height: 60px;
            width: 60px;
            margin-bottom: 5px;
        }
        fieldset {
            border: 2px solid #383843;
            border-radius: 10px;
        }
        legend {
            font-weight: bold;
            width: auto;
        }
        .col_box{
            border: 2px solid #b5b5c6;
            border-radius: 10px;
        }
        .i-checks {
            margin-left: 5% !important;
        }
        select{width: 100% !important;}
    </style>

</head>

<body>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header" style="text-align: center;">
                    <div class="dropdown profile-element">
                        <img alt="image" class="rounded-circle pp_img" src="<?php echo base_url()?>asset/img/user_icon.png"/>
                    </div>
                </li>
                <li class="home">
                    <a href="<?php echo base_url()?>welcome"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span></a>
                </li>
                <?php if ($_SESSION['user_type'] == 'admin') { ?>
                <li class="user">
                    <a href="<?php echo base_url()?>welcome/user"><i class="fa fa-th-large"></i> <span class="nav-label">User</span></a>
                </li>
                <li class="course">
                    <a href="<?php echo base_url()?>welcome/course"><i class="fa fa-th-large"></i> <span class="nav-label">Course</span></a>
                </li>
                <li class="lesson">
                    <a href="<?php echo base_url()?>welcome/lesson"><i class="fa fa-th-large"></i> <span class="nav-label">Lesson</span></a>
                </li>
                <?php } else{ ?>
                <li class="history">
                    <a href="<?php echo base_url()?>welcome/history"><i class="fa fa-th-large"></i> <span class="nav-label">History</span></a>
                </li>
                <?php } ?>
                <li class="ranking">
                    <a href="<?php echo base_url()?>welcome/ranking"><i class="fa fa-th-large"></i> <span class="nav-label">Ranking</span></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <?php
                        if(isset($_SESSION['name'])){
                            $user_name = $_SESSION['name'];
                        }else{
                            $user_name = 'Admin';
                        }
                    ?>
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Welcome <?php echo $user_name ?></span>
                    </li>
                    <li>
                        <a href="<?php echo base_url()."login/logout" ?>">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

