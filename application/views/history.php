<?php include_once 'includes/header.php'; ?>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>History</h5>
                        </div>
                        <div class="ibox-content">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>COURSE</th>
                                    <th>LESSON</th>
                                    <th>QUESTION_SOLVED</th>
                                    <th>RIGHT</th>
                                    <th>WRONG</th>
                                    <th>SCORE</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($history)) {
                                    foreach ($history as $key => $his) { ?>
                                        <tr>
                                            <td><?php echo $key + 1; ?></td>
                                            <td><?php echo $his['title']; ?></td>
                                            <td><?php echo $his['lesson']; ?></td>
                                            <td><?php echo $his['level']; ?></td>
                                            <td><?php echo $his['right']; ?></td>
                                            <td><?php echo $his['wrong']; ?></td>
                                            <td><?php echo $his['score']; ?></td>
                                        </tr>
                                    <?php }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include_once 'includes/footer.php'; ?>
</body>

</html>
