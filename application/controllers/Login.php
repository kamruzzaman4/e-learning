<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class Login extends CI_Controller {
    public function index(){
        if(isset($_POST)) {
            $type = $_POST['user_type'];
            $email = $_POST['email'];
            $pass = $_POST['password'];
            $email = stripslashes($email);
            $email = addslashes($email);
            $pass = stripslashes($pass);
            $pass = addslashes($pass);

            $user = $this->db->query("SELECT * FROM $type WHERE email='$email' and password='$pass'")->row();

            if (!empty($user)){
                $_SESSION['logged'] = $email;
                $_SESSION['user_type'] = $type;
                $_SESSION['name'] = $user->name;
                if ($type == 'admin') {
                    $_SESSION['id'] = $user->id;
                }
                $_SESSION['email'] = $user->email;
                $_SESSION['password'] = $user->password;
                //$this->load->view('welcome');
                redirect('/Welcome/home');
            }else{
                echo "<center><h3><script>alert('Sorry.. Wrong Username (or) Password');</script></h3></center>";
                $data['title'] = $type;
                $this->load->view('login',$data);
            }
        }
    }

    public function register(){
        if(isset($_POST)) {
            $name = $_POST['name'];
            $name = stripslashes($name);
            $name = addslashes($name);

            $email = $_POST['email'];
            $email = stripslashes($email);
            $email = addslashes($email);

            $password = $_POST['password'];
            $password = stripslashes($password);
            $password = addslashes($password);

            $college = $_POST['college'];
            $college = stripslashes($college);
            $college = addslashes($college);
            $user = $this->db->query("SELECT email from user WHERE email='$email'")->row();
            if (!empty($user)){
                echo "<center><h3><script>alert('Sorry.. This email is already registered !!');</script></h3></center>";
                redirect('/Welcome/register');
            }else{
                $userData = [
                    'name' => $name,
                    'email' => $email,
                    'password' => $password,
                    'college' => $college
                ];
                $this->db->insert('user', $userData);

                echo "<center><h3><script>alert('Congrats.. You have successfully registered !!');</script></h3></center>";
                $_SESSION['logged'] = $email;
                $_SESSION['user_type'] = 'user';
                $_SESSION['name'] = $user->name;
                $_SESSION['email'] = $user->email;
                $_SESSION['password'] = $user->password;
                redirect('/Welcome/home');
            }
        }
    }

    public function logout(){
        if(isset($_SESSION)){
            $user_type = $_SESSION['user_type'];
            session_destroy();
            if ($user_type == 'admin'){
                redirect('/Welcome/admin');
            }else{
                redirect('/');
            }
        }
    }
}
