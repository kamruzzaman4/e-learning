<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Data extends CI_Controller {
    public function delete_user(){
        $deleteArr = ['email' => $_GET['demail']];
        $this->db->delete('rank', $deleteArr);
        $this->db->delete('history', $deleteArr);
        $this->db->delete('user', $deleteArr);

        redirect('welcome/user');
    }

    public function course_entry(){
        if(isset($_POST)) {
            $dataArr = [
                'title' => $_POST['name']
            ];
            $this->db->insert('course', $dataArr);

            echo "<center><h3><script>alert('Congrats.. You have successfully inserted !!');</script></h3></center>";
            redirect('/welcome/course');

        }
    }

    public function delete_course(){
        $id = $_GET['id'];
        $this->db->delete('quiz', ['course_id' => $id]);
        $this->db->delete('course', ['id' => $id]);

        redirect('welcome/course');
    }

    public function delete_lesson(){
        $eid = $_GET['id'];
        $questions = $this->db->query("SELECT qid FROM questions WHERE eid = '$eid'")->result_array();
        //echo "<pre>"; print_r($questions); exit;
        if (!empty($questions)){
            foreach ($questions as $ques){
                $this->db->delete('options', ['qid' => $ques['qid']]);
                $this->db->delete('answer', ['qid' => $ques['qid']]);
            }
        }

        $this->db->delete('questions', ['eid' => $eid]);
        $this->db->delete('quiz', ['eid' => $eid]);
        $this->db->delete('history', ['eid' => $eid]);

        redirect('welcome/lesson');
    }

    public function save_lesson_n_mcq(){
        //echo "<pre>"; print_r($_POST); exit;
        if(isset($_POST)) {
            $no_question = $_POST['no_question'];
            $lessonArr = [
                'course_id' => $_POST['course_id'],
                'title' => $_POST['lesson_name'],
                'right' => $_POST['right_ans'],
                'wrong' => $_POST['wrong_ans'],
                'total' => $no_question
            ];
            $this->db->insert('quiz', $lessonArr);
            $quiz_id = $this->db->insert_id();

            $eid = $quiz_id;
            $ch = 4;
            for($i=1; $i<=$no_question; $i++){
                $qid = uniqid();
                $qns = $_POST['qns'.$i];
                $questionArr = [
                    'eid' => $eid,
                    'qid' => $qid,
                    'qns' => $qns,
                    'choice' => $ch,
                    'sn' => $i
                ];
                $this->db->insert('questions', $questionArr);

                $oaid = uniqid(); $obid = uniqid(); $ocid = uniqid(); $odid = uniqid();
                $a = $_POST[$i.'1'];
                $b = $_POST[$i.'2'];
                $c = $_POST[$i.'3'];
                $d = $_POST[$i.'4'];

                $this->db->insert('options', ['qid' => $qid, 'option' => $a, 'optionid' => $oaid]);
                $this->db->insert('options', ['qid' => $qid, 'option' => $b, 'optionid' => $obid]);
                $this->db->insert('options', ['qid' => $qid, 'option' => $c, 'optionid' => $ocid]);
                $this->db->insert('options', ['qid' => $qid, 'option' => $d, 'optionid' => $odid]);

                $e = $_POST['ans'.$i];
                switch($e){
                    case 'a': $ansid = $oaid; break;
                    case 'b': $ansid = $obid; break;
                    case 'c': $ansid = $ocid; break;
                    case 'd': $ansid = $odid; break;
                    default: $ansid = $oaid;
                }
                $this->db->insert('answer', ['qid' => $qid, 'ansid' => $ansid]);
            }
            redirect('welcome/lesson');
        }
    }

    public function quiz(){
        if(!(isset($_SESSION['email']))){
            redirect('/');
        }else {
            if ($_SESSION['user_type'] == 'user') {
                $eid = $_GET['eid'];
                $sn = $_GET['n'];
                $total = $_GET['t'];
                $re = isset($_GET['re']) ? $_GET['re'] : 0;
                $email = $_SESSION['email'];
                if($re == 1) {
                    $q = $this->db->query("SELECT score FROM history WHERE eid='$eid' AND email='$email'")->result_array();
                    foreach ($q as $row) {
                        $s = $row['score'];
                    }
                    $deleteHistoryArr = ['eid' => $eid, 'email' => $email];
                    $this->db->delete('history', $deleteHistoryArr);
                    $q = $this->db->query("SELECT * FROM `rank` WHERE email='$email'")->result_array();
                    foreach ($q as $row) {
                        $sun = $row['score'];
                    }
                    $this->load->helper('date');
                    $sun = $sun - $s;
                    $rankUpdateArr = ['score' => $sun, 'time' => NOW()];
                    $this->db->where('email', $email);
                    $this->db->update('rank', $rankUpdateArr);
                }

                $html = '<div class="panel" style="margin:1%">';

                $questions = $this->db->query("SELECT * FROM questions WHERE eid='$eid' AND sn='$sn'")->result_array();
                foreach ($questions as $ques){
                    $qns = $ques['qns'];
                    $qid = $ques['qid'];
                    $html .= '<h2>Question &nbsp;'.$sn.'&nbsp;:: '.$qns.'</h2>';
                }
                $options = $this->db->query("SELECT * FROM options WHERE qid='$qid'")->result_array();
                $url = base_url().'data/next_quiz';
                $html .= '<form action="'.$url.'?eid='.$eid.'&n='.$sn.'&t='.$total.'&qid='.$qid.'&re='.$re.'" method="POST"  class="form-horizontal">
                        <br />';
                if (!empty($options)){
                    foreach ($options as $opt){
                        $option = $opt['option'];
                        $optionid = $opt['optionid'];
                        $html .= '<input type="radio" name="ans" value="'.$optionid.'">&nbsp;'.$option.'<br /><br />';
                    }
                }
                $html .= '<br /><button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>&nbsp;Submit</button></form></div>';
                $data['html'] = $html;
                $this->load->view('quiz',$data);
            }else{
                redirect('/');
            }
        }
    }

    public function next_quiz(){
        $eid = $_GET['eid'];
        $sn = $_GET['n'];
        $total = $_GET['t'];
        $ans = $_POST['ans'];
        $qid = $_GET['qid'];
        $re = $_GET['re'];
        $email = $_SESSION['email'];

        $this->load->helper('date');

        //echo "<pre>"; print_r($_GET); exit;

        $q = $this->db->query("SELECT * FROM answer WHERE qid='$qid'")->result_array();
        foreach ($q as $row) {$ansid = $row['ansid'];}
        if($ans == $ansid){
            $q = $this->db->query("SELECT * FROM quiz WHERE eid='$eid'")->result_array();
            foreach ($q as $row) {$right=$row['right'];}
            if($sn == 1){
                $historyArr = ['email'=>$email,'eid'=>$eid,'score'=>0,'level'=>0,'right'=>0,'wrong'=>0,'date'=>NOW()];
                $this->db->insert('history', $historyArr);
            }
            $q = $this->db->query("SELECT * FROM history WHERE eid='$eid' AND email='$email'")->result_array();
            foreach ($q as $row){
                $s = $row['score'];
                $r = $row['right'];
            }
            $r++;
            $s = $s+$right;
            $historyUpdateArr = ['score'=>$s,'level'=>$sn,'right'=>$r,'date'=>NOW()];
            //$this->db->query("UPDATE `history` SET `score`=$s,`level`=$sn,`right`=$r, date= NOW()  WHERE  email = '$email' AND eid = '$eid'");
            $this->db->where('email', $email);
            $this->db->where('eid', $eid);
            $this->db->update('history', $historyUpdateArr);
        }else{
            $q = $this->db->query("SELECT * FROM quiz WHERE eid='$eid'")->result_array();
            foreach ($q as $row){
                $wrong=$row['wrong'];
            }
            if($sn == 1){
                $historyArr = ['email'=>$email,'eid'=>$eid,'score'=>0,'level'=>0,'right'=>0,'wrong'=>0,'date'=>NOW()];
                $this->db->insert('history', $historyArr);
            }
            $q = $this->db->query("SELECT * FROM history WHERE eid='$eid' AND email='$email'")->result_array();
            foreach ($q as $row){
                $s = $row['score'];
                $w = $row['wrong'];
            }
            $w++;
            $s = $s-$wrong;
            $historyUpdateArr = ['score'=>$s,'level'=>$sn,'wrong'=>$w,'date'=>NOW()];
            $this->db->where('email', $email);
            $this->db->where('eid', $eid);
            $this->db->update('history', $historyUpdateArr);
        }

        if($sn != $total){
            $sn++;
            header("location:quiz?eid=$eid&n=$sn&t=$total");
        }
        else if( $_SESSION['user_type'] != 'admin'){
            $q = $this->db->query("SELECT score FROM history WHERE eid='$eid' AND email='$email'")->result_array();
            foreach ($q as $row){
                $s = $row['score'];
            }
            $q = $this->db->query("SELECT * FROM `rank` WHERE email='$email'")->result_array();
            if(empty($q)){
                $rankArr = ['email'=>$email,'score'=>$s,'time'=>NOW()];
                $this->db->insert('rank', $rankArr);
            }
            else{
                foreach ($q as $row){
                    $sun=$row['score'];
                }
                $sun = $s+$sun;
                $this->db->where('email', $email);
                $this->db->update('rank', ['score'=>$sun,'time'=>NOW()]);
            }

            //header("location:welcome.php?q=result&eid=$eid");
            $results = $this->db->query("SELECT * FROM history WHERE eid='$eid' AND email='$email'")->result_array();
            $resultHtml = '<div class="panel"><h1 class="title text-center" style="color:#660033">Result</h1><br /><table class="table table-striped title1" style="font-size:20px;font-weight:1000;">';
            if(!empty($results)){
                foreach ($results as $row){
                    $s = $row['score'];
                    $w = $row['wrong'];
                    $r = $row['right'];
                    $qa = $row['level'];
                    $resultHtml .= '<tr style="color:#66CCFF"><td>Total Questions</td><td>'.$qa.'</td></tr><tr style="color:#99cc32"><td>Right Answer&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td><td>'.$r.'</td></tr><tr style="color:red"><td>Wrong Answer&nbsp;<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></td><td>'.$w.'</td></tr><tr style="color:#66CCFF"><td>Score&nbsp;<span class="glyphicon glyphicon-star" aria-hidden="true"></span></td><td>'.$s.'</td></tr>';
                }
            }

            $user_rank = $this->db->query("SELECT * FROM `rank` WHERE  email='$email'")->result_array();
            foreach ($user_rank as $row){
                $s = $row['score'];
                $resultHtml .= '<tr style="color:#990000"><td>Overall Score&nbsp;<span class="glyphicon glyphicon-stats" aria-hidden="true"></span></td><td>'.$s.'</td></tr>';
            }
            $resultHtml .= '</table></div>';
            $data['resultHtml'] = $resultHtml;
            $this->load->view('result',$data);
        }
        else{
            //header("location:welcome.php?q=result&eid=$eid");
            $results = $this->db->query("SELECT * FROM history WHERE eid='$eid' AND email='$email'")->result_array();
            $resultHtml = '<div class="panel"><h1 class="title text-center" style="color:#660033">Result</h1><br /><table class="table table-striped title1" style="font-size:20px;font-weight:1000;">';
            if(!empty($results)){
                foreach ($results as $row){
                    $s = $row['score'];
                    $w = $row['wrong'];
                    $r = $row['right'];
                    $qa = $row['level'];
                    $resultHtml .= '<tr style="color:#66CCFF"><td>Total Questions</td><td>'.$qa.'</td></tr><tr style="color:#99cc32"><td>Right Answer&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td><td>'.$r.'</td></tr><tr style="color:red"><td>Wrong Answer&nbsp;<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></td><td>'.$w.'</td></tr><tr style="color:#66CCFF"><td>Score&nbsp;<span class="glyphicon glyphicon-star" aria-hidden="true"></span></td><td>'.$s.'</td></tr>';
                }
            }

            $user_rank = $this->db->query("SELECT * FROM `rank` WHERE  email='$email'")->result_array();
            foreach ($user_rank as $row){
                $s = $row['score'];
                $resultHtml .= '<tr style="color:#990000"><td>Overall Score&nbsp;<span class="glyphicon glyphicon-stats" aria-hidden="true"></span></td><td>'.$s.'</td></tr>';
            }
            $resultHtml .= '</table></div>';
            $data['resultHtml'] = $resultHtml;
            $this->load->view('result',$data);
        }
    }

}
