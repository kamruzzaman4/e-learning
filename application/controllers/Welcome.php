<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Welcome extends CI_Controller {
	public function index(){
        if(!(isset($_SESSION['email']))){
            $data['title'] = 'user';
            $this->load->view('login',$data);
        }else{
            redirect('/Welcome/home');
        }
	    /*$data['title'] = 'user';
        $this->load->view('login',$data);*/
	}

	public function admin(){
        $data['title'] = 'admin';
        $this->load->view('login',$data);
    }

    public function register(){
        $this->load->view('register');
    }

    public function home(){
        if(!(isset($_SESSION['email']))){
            redirect('/');
        }else {
            $email = $_SESSION['email'];
            $data['courses'] = $this->db->query("SELECT * FROM course")->result_array();
            $lessons = $this->db->query("SELECT quiz.*, history.eid AS history FROM quiz LEFT JOIN history ON history.eid = quiz.eid AND history.email = '$email' ORDER BY quiz.date DESC")->result_array();
            $lessonArr = [];
            if (!empty($lessons)){
                foreach ($lessons as $lesson) {
                    $lessonArr[$lesson['course_id']][$lesson['eid']] = [
                        'title' => $lesson['title'],
                        'total' => $lesson['total'],
                        'right' => $lesson['right'],
                        'history' => $lesson['history']
                    ];
                }
            }
            //echo "<pre>"; print_r($lessonArr);exit;
            $data['lessons'] = $lessonArr;

            $this->load->view('welcome',$data);
        }
    }

    public function user(){
        if(!(isset($_SESSION['email']))){
            redirect('/');
        }else {
            if ($_SESSION['user_type'] == 'admin') {
                $data['users'] = $this->db->query('SELECT * FROM user')->result_array();
                $this->load->view('user',$data);
            }else{
                redirect('/');
            }
        }
    }

    public function course(){
        if(!(isset($_SESSION['email']))){
            redirect('/');
        }else {
            if ($_SESSION['user_type'] == 'admin') {
                $data['courses'] = $this->db->query('SELECT * FROM course')->result_array();
                $this->load->view('course',$data);
            }else{
                redirect('/');
            }
        }
    }

    public function lesson(){
        if(!(isset($_SESSION['email']))){
            redirect('/');
        }else {
            if ($_SESSION['user_type'] == 'admin') {
                $data['courses'] = $this->db->query('SELECT * FROM course')->result_array();
                $data['lessons'] = $this->db->query('SELECT course.title AS courese_title, quiz.* FROM quiz INNER JOIN course ON course.id = quiz.course_id ORDER BY quiz.date DESC')->result_array();
                $this->load->view('lesson',$data);
            }else{
                redirect('/');
            }
        }
    }

    public function ranking(){
        if(!(isset($_SESSION['email']))){
            redirect('/');
        }else {
            $data['ranking'] = $this->db->query('SELECT user.name, user.college, user.email, rank.score FROM `rank` INNER JOIN user ON user.email = rank.email ORDER BY score DESC')->result_array();
            $this->load->view('ranking',$data);
        }
    }

    public function history(){
        if(!(isset($_SESSION['email']))){
            redirect('/');
        }else {
            if ($_SESSION['user_type'] == 'user') {
                $email = $_SESSION['email'];
                $data['history'] = $this->db->query("SELECT course.title, quiz.title AS lesson, history.* FROM history INNER JOIN quiz ON quiz.eid = history.eid INNER JOIN course ON course.id = quiz.course_id WHERE email='$email' ORDER BY history.date DESC")->result_array();
                $this->load->view('history',$data);
            }else{
                redirect('/');
            }
        }
    }
}
